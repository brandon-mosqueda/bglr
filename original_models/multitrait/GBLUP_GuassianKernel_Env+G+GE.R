rm(list=ls(all=TRUE))
library(BGLR)
library(BMTME)
library(dplyr)

load('MaizeToy.RData',verbose=TRUE)

dat_F =phenoMaizeToy
head(dat_F)

Y = as.matrix(dat_F[,-(1:2)])

G =genoMaizeToy
J =  dim(G)[1]
dfE=2 ####Number of environments -1
XE =  matrix(model.matrix(~0+as.factor(Env),data=dat_F)[,-1],nc=dfE)
dim(XE)
Z  =  model.matrix(~0+as.factor(Line),data=dat_F)
KG =  Z%*%G%*%t(Z)
L=cholesky(KG)

#########GE interaction term
KE=XE%*%t(XE)
KGE=KG*KE
LGE=cholesky(KGE)

l2norm=function(x){sqrt(sum(x^2))}
K.radial=function(x1,x2=x1, gamma=1){
  exp(-gamma*outer(1:nrow(x1 <- as.matrix(x1)), 1:ncol(x2 <- t(x2)),
                   Vectorize(function(i, j) l2norm(x1[i,]-x2[,j]) ^2)))}

KK=K.radial(L)
KKGE=K.radial(LGE)
#Partitions for a 5-FCV
n=dim(phenoMaizeToy)[1]
nCV=5
set.seed(1)
####Training testing sets using the BMTME package###############
pheno <- data.frame(GID = phenoMaizeToy[, 1], Env = phenoMaizeToy[, 2],
                    Response = phenoMaizeToy[, 3])
nCV=5
CrossV <- CV.KFold(pheno, DataSetID = 'GID', K = nCV, set_seed = 123)

#Predictor BGLR
ETA = list(list(X=XE,model='FIXED'),list(K=KK,model='RKHS'),list(K=KKGE,model='RKHS')) 
#Function to summarize the performance prediction: PC_MM_f
source('PC_MM.R')#See below
Tab =  data.frame()
set.seed(1)
for(p in 1:5)
{
#p=1
  Y_NA = Y
  Pos_NA =CrossV$CrossValidation_list[[p]]
  Y_NA[Pos_NA,] = NA
  A = Multitrait(y = Y_NA, ETA=ETA,
                 resCov = list( type = 'UN',  S0 = diag(3), df0 = 5 ),
                 nIter =15000,  burnIn = 10000)
  PC  = PC_MM_f(Y[Pos_NA,],A$yHat[Pos_NA,],Env=dat_F$Env[Pos_NA])
  Tab = rbind(Tab,data.frame(PT=p,PC))
  cat('PT=',p,'\n')
}
Tab_R =  Tab%>%group_by(Env,Trait)%>%select(Cor,MSEP)%>%summarise(Cor_mean = mean(Cor),
                                                                  Cor_sd    = sd(Cor),
                                                                  MSEP_mean = mean(MSEP),
                                                                  MSEP_sd =  sd(MSEP))
Tab_R =  as.data.frame(Tab_R)
Tab_R
