rm(list=ls(all=TRUE))

library(BGLR)
library(BMTME)
library(dplyr)

load('MaizeToy.RData', verbose=TRUE)

K.polynomial <- function(x1, x2=x1, gamma=1, b=0, p=3) {
  return((gamma * (as.matrix(x1) %*% t(x2)) + b)^p)
}

dat_F <- phenoMaizeToy
head(dat_F)

Y <- as.matrix(dat_F[, -(1:2)])

G <- genoMaizeToy
J <- nrow(G)

XE <- model.matrix(~0 + as.factor(Env), data=dat_F)
dim(XE)
Z <- model.matrix(~0 + as.factor(Line), data=dat_F)
LG <- cholesky(G)
LG_K <- K.polynomial(LG)
Z.G_kernel<- Z %*% LG_K

ZEG <- model.matrix(~0 + Z.G_kernel:as.factor(phenoMaizeToy$Env))
ZEG_kernel <-ZEG

#Partitions for a 5-FCV
n <- nrow(phenoMaizeToy)
nCV <- 5
set.seed(1)

####Training testing sets using the BMTME package###############
pheno <- data.frame(GID=phenoMaizeToy[, 1], Env=phenoMaizeToy[, 2],
                    Response=phenoMaizeToy[, 3])

CrossV <- CV.KFold(pheno, DataSetID='GID', K=nCV, set_seed=123)

Y <- as.matrix(phenoMaizeToy[, -c(1, 2)])
pm <- BMTME(Y=Y, X=XE, Z1=Z.G_kernel, Z2=ZEG_kernel, nIter=6000,
            burnIn=4000, thin=2, bs=50, testingSet=CrossV)
results <-pm$results

Pred_Summary <- summary.BMTMECV(results=results,
                                information='compact',
                                digits=4)
Pred_Summary