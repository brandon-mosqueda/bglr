rm(list=ls(all=TRUE))

library(plyr)
library(tidyr)
library(dplyr)
library(glmnet)
library(BMTME)

setwd("~/Data_Science/bglr")

load("datasets/Groundnut_data.RData", verbose=TRUE)
ls()
Pheno=Pheno_Groundnut
dim(Pheno)
XF=Markers_Groundnut[,-1]
dim(XF)
head(XF[,1:5])

########Linear Kernel############
K.linear=function(x1, x2=x1,gamma=1) {gamma*(as.matrix(x1)%*%t(as.matrix(x2))) }
K.lin=K.linear(x1=XF,x2=XF,gamma=1/ncol(XF))
dim(K.lin)

########Polynomial Kernel############
K.polynomial=function(x1, x2=x1, gamma=1, b=0, p=3){
  (gamma*(as.matrix(x1)%*%t(x2))+b)^p}

K.poly=K.polynomial(x1=XF,x2=XF,gamma=1/ncol(XF))
dim(K.poly)


########Sigmoid Kernel############
K.sigmoid=function(x1,x2=x1, gamma=1/ncol(XF), b=0)
{ tanh(gamma*(as.matrix(x1)%*%t(x2))+b) }
K.sig=K.sigmoid(x1=XF,x2=XF, gamma=1/1*ncol(XF))
dim(K.sig)

########Radial Kernel############
l2norm=function(x){sqrt(sum(x^2))}
K.radial=function(x1,x2=x1, gamma=1){
  exp(-gamma*outer(1:nrow(x1 <- as.matrix(x1)), 1:ncol(x2 <- t(x2)),
                   Vectorize(function(i, j) l2norm(x1[i,]-x2[,j])^2)))}
K.rad=K.radial(x1=XF,x2=XF, gamma=1/ncol(XF))
dim(K.rad)

########Exponencial Kernel############
K.exponential=function(x1,x2=x1, gamma=1){
  exp(-gamma*outer(1:nrow(x1 <- as.matrix(x1)), 1:ncol(x2 <- t(x2)),
                   Vectorize(function(i, j) l2norm(x1[i,]-x2[,j]))))}

K.exp=K.exponential(x1=XF,x2=XF, gamma=1/ncol(XF))
dim(K.exp)

########Design matrices###
ZE=model.matrix(~0+as.factor(Pheno$ENV))
ZL=model.matrix(~0+as.factor(Pheno$GID))
KG=  ZL%*%K.rad%*%t(ZL) ####Aqui tienes que cambiar a cada uno de los 5 kernels
KE=ZE%*%t(ZE)
KGE=KG*KE

#Partitions for a 5-FCV
n <- nrow(Pheno)
nCV <- 5
set.seed(1)

####Training testing sets using the BMTME package###############
pheno <- data.frame(GID=Pheno[, 1], Env=Pheno[, 2],
                    Response=Pheno[, 3])

CrossV <- CV.KFold(pheno, DataSetID='GID', K=nCV, set_seed=123)

Y <- as.matrix(Pheno[, -c(1, 2)])

#Predictor BGLR
ETA = list(list(X=ZE,model='FIXED'),list(K=KG,model='RKHS'),list(K=KGE,model='RKHS'))
#Function to summarize the performance prediction: PC_MM_f
source('PC_MM.R')#See below
Tab =  data.frame()
set.seed(1)
for(p in 1:5)
{
  #p=1
  Y_NA = Y
  Pos_NA =CrossV$CrossValidation_list[[p]]
  Y_NA[Pos_NA,] = NA
  A = Multitrait(y = Y_NA, ETA=ETA,
                 resCov = list( type = 'UN',  S0 = diag(4), df0 = 5 ),
                 nIter =15000,  burnIn = 10000)
  PC  = PC_MM_f(Y[Pos_NA,],A$yHat[Pos_NA,],Env=dat_F$Env[Pos_NA])
  Tab = rbind(Tab,data.frame(PT=p,PC))
  cat('PT=',p,'\n')
}
Tab_R =  Tab%>%group_by(Env,Trait)%>%select(Cor,MSEP)%>%summarise(Cor_mean = mean(Cor),
                                                                  Cor_sd    = sd(Cor),
                                                                  MSEP_mean = mean(MSEP),
                                                                  MSEP_sd =  sd(MSEP))
Tab_R =  as.data.frame(Tab_R)
Tab_R
