rm(list=ls(all=TRUE))

library(BGLR)
library(BMTME)
library(dplyr)

# setwd("/home/oamontes/bglr")
# setwd("/home/raymundob/bglr")
setwd("/mnt/zfs-pool/home/abelardo.montesinos/OsvalM/Brandon/bglr")
# setwd("~/bglr")
# setwd("~/Data_Science/bglr")
# setwd("/mnt/zfs-pool/home/osval.montes/Osval/Brandon/bglr")

# Function to summarize the performance prediction and other useful
# own functions.
source("utils.R")

set.seed(1)

digits <- 4
groups <- get_datasets_files(c("gblup", "gblup_polynomial",
                               "gblup_sigmoid", "gblup_gaussian",
                               "gblup_exponential", "gblup_arc_cosine_1",
                               "gblup_arc_cosine_2", "gblup_arc_cosine_3",
                               "gblup_arc_cosine_4"))
groups_names <- c("gblup_exponential", "gblup_arc_cosine_1",
                  "gblup_arc_cosine_2", "gblup_arc_cosine_3",
                  "gblup_arc_cosine_4")
groups_names <- c(groups_names, paste0(groups_names, "_interaction"))
groups <- get_datasets_files(groups_names)
# groups <- get_datasets_files("gblup_arc_cosine_1")
groups <- get_datasets_files("brr_interaction")

for (i in 1:length(groups)) {
  # remain_index <- which(sapply(groups[[i]], has_str, substring="Peanut"))
  # remain_index <- which(sapply(groups[[i]], has_str, substring="Groundnut"))
  remain_index <- which(sapply(groups[[i]], has_str, substring="Chickpea"))
  # remain_index <- which(sapply(groups[[i]], has_str, substring="Data_EYT_Set_6"))
  groups[[i]] <- groups[[i]][remain_index]
}
print(groups)

groups_names <- names(groups)

# group_index <- 1
# file_index <- 1
# k <- 1
for (group_name in groups_names) {
  # group_name <- groups_names[[group_index]]
  cat(group_name, ": \n")
  datasets_files <- groups[[group_name]]

  for (dataset_file in datasets_files) {
    # dataset_file <- datasets_files[file_index]
    load(dataset_file, Data <- new.env())
    cat("\t", Data$info$name, ":\n")
    n_responses <- ncol(Data$Y)

    # Partitions for a 5-FCV
    nCV <- 5

    # Training testing sets using the BMTME package
    pheno_for_cv <- data.frame(GID=Data$pheno$GID,
                               Env=Data$pheno$Env,
                               Response=Data$pheno[, 3])

    CrossV <- CV.KFold(pheno_for_cv, DataSetID='GID',
                       K=nCV, set_seed=123)

    InnerTab <- data.frame()

    for(k in 1:nCV) {
      Y_NA <- Data$Y
      Pos_NA <- CrossV$CrossValidation_list[[k]]
      Y_NA[Pos_NA, ] <- NA

      A <- Multitrait(y=Y_NA, ETA=Data$ETA,
                      resCov=list(type='UN',
                                  S0=diag(n_responses),
                                  df0=5),
                      nIter=15000, burnIn=10000,
                      verbose=FALSE,
                      saveAt=paste0(Data$info$name, "_", group_name))

      Predictions <- A$ETAHat
      if (is.null(Predictions)) {
        Predictions <- A$yHat
      }

      PC <- PC_MM_f(Data$Y[Pos_NA, ],
                    Predictions[Pos_NA, ],
                    Env=Data$pheno$Env[Pos_NA])

      InnerTab <- rbind(InnerTab, data.frame(PT=k, PC))
      cat('\t\tk = ', k, ', ', sep="")
    }
    cat("\n")

    InnerTabResults <- InnerTab %>% group_by(Env, Trait) %>%
                            select(Cor, MSEP) %>%
                            summarise(Cor_mean=mean(Cor),
                                      Cor_sd=sd(Cor) / sqrt(nCV),
                                      MSEP_mean=mean(MSEP),
                                      MSEP_sd=sd(MSEP) / sqrt(nCV))
    InnerTabResults <- as.data.frame(InnerTabResults)
    InnerTabResults$dataset <- Data$info$name

    write.csv(InnerTabResults, file=paste0("results/", Data$info$name, "_",
                                           group_name, ".csv"),
              row.names=FALSE)
  }
}