#!/bin/bash

#SBATCH --job-name=test    # Job name
#SBATCH --ntasks=10                    # Run on a n CPUs
#SBATCH --mem=15gb                     # Job memory request
#SBATCH -o ../output/test.out       # No poner números
#SBATCH -e ../error/test.err       # No poner números
#SBATCH --mail-type=END,FAIL          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=bmosqueda@ucol.mx     # Where to send mail

## You must run the command f
### Aqui inicia la ejecución de su codigo #####
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cm/shared/apps/R-3.6.1/lib64/R/lib:/usr/lib64

cd /home/oamontes/bglr/run/
# cd /home/raymundob/bglr/run/

R CMD BATCH test.R