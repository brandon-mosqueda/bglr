#!/bin/bash
#SBATCH --job-name=unitrait_analysis
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=bmosqueda@ucol.mx
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=15gb
#SBATCH --output=unitrait_%j.log

module load R-3.6.1
cd "/home/oamontes/bglr"
R CMD BATCH unitrait_main.sh