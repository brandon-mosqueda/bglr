#!/bin/bash

#PBS -N chickpea_bmtme_sigmoid
#PBS -l nodes=1:ppn=64,mem=30gb
#PBS -q staff
#PBS -d /mnt/zfs-pool/home/osval.montes/Osval/Brandon/bglr/run
#PBS -o chickpea_bmtme_sigmoid.log
#PBS -j oe
#PBS -V
#PBS -S /bin/bash
#PBS -m ea                    # Send mail on end and abort
#MAILTO="bmosqueda@ucol.mx"   # default is the user submitting the job

# Habilitar conda
source ~/anaconda3/etc/profile.d/conda.sh

# Habilitar env conda
conda activate tf_2

# Correr script
R CMD BATCH chickpea_bmtme_sigmoid.R