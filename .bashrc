alias rm='rm --interactive=never'
alias cd-1='cd ../'
alias cd-2='cd ../../'
alias cd-3='cd ../../../'

alias ldir='ls -d */'

alias topu='top -u $(whoami)'
alias qstatu='qstat -u $(whoami)'
alias squeueu='squeue -u $(whoami)'

alias gss='git status -s'
alias ga='git add'
alias gaa='git add --all'
alias gca='git commit -v -a'
alias gc='git commit -v'
alias gd='git diff'
alias grst='git restore --staged'