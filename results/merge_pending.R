rm(list=ls(all=TRUE))

setwd("~/Data_Science/bglr/results")

source("../utils.R")

get_model <- function(file_name) {
  # regex <- "(gblup|bmtme|brr)_[a-z_1-9]+(_interaction)?"
  regex <- "(gblup|bmtme|brr)(_)?[a-z_1-9]*(_interaction)?(?=.csv)"
  model <- regex_match(file_name, regex)

  if (is.null(model)) {
    model <- regex_match(file_name, "(?<=_)bmtme(?=.csv)")
  } else if (has_str(model, "_linear_?")) {
    model <- gsub("_linear_?", "", model)
  }

  return(model)
}

get_type <- function(file_name) {
  return(regex_match(file_name, "unitrait|multitrait|partial_multitrait"))
}

get_append_file_name <- function(file_name) {
  type <- get_type(file_name)
  model <- get_model(file_name)

  return(file.path(type, paste0(model, ".csv")))
}

get_dataset <- function(file_name) {
  if (has_str(tolower(file_name), "chickpea")) {
    return("Chickpea_data.RData")
  } else if (has_str(tolower(file_name), "groundnut")) {
    return("Groundnut_data.RData")
  } else {
    print(file_name)
    stop("Not valid dataset")
  }
}

pending_files <- list.files("pending", recursive=TRUE, full.names=TRUE)

for (file_name in pending_files) {
  PendingData <- read.csv(file_name)
  append_to_file <- get_append_file_name(file_name)

  if (file.exists(append_to_file)) {
    MergeResults <- read.csv(append_to_file, stringsAsFactors=FALSE)
    MergeResults$X <- NULL
    MergeResults$Num <- NULL
  } else {
    MergeResults <- data.frame()
    print(append_to_file)
  }

  MergeResults <- rbind(MergeResults, PendingData)
  write.csv(MergeResults, file=append_to_file, row.names=FALSE)
  unlink(file_name)
}
